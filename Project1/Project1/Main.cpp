#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include"FillColor.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE
	//SDL_Color red,blue;
	//red = { 255, 0, 0,255 };
	//blue = { 0, 87, 249, 255 };
	SDL_SetRenderDrawColor(ren, 0, 87, 249, 255);
	Vector2D v1(100,100),v2(100,400),v3(500,600),v4(200,115);
	DrawCurve2(ren, v1, v2, v3);

	SDL_SetRenderDrawColor(ren,255, 0, 0, 255);
	SDL_RenderDrawPoint(ren, v1.x, v1.y);
	SDL_RenderDrawPoint(ren, v2.x, v2.y);
	SDL_RenderDrawPoint(ren, v3.x, v3.y);

	//Uint32 pixel_fomat = SDL_GetWindowPixelFormat(win);
	////BoundaryFill4(win, v4, pixel_fomat, ren, red, blue);
	////RectangleFill(v1, v3, ren, red);
	//SDL_RenderClear(ren);
	


	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event


	bool running = true, isMouseSelection, flag1 = false, flag2 = false, flag3 = false, flag4 = false;
	while (running)
	{
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT: running = false; break;
			case SDL_MOUSEBUTTONUP:

				// If the left mouse button release, reset flag to false! 
				if (event.button.button == SDL_BUTTON_LEFT)
				{

					//Debuggin: Get the mouse offsets 
					cout << "Mouse released at: " << event.button.x << "," << event.button.y << endl;
					int x = event.button.x;
					int y = event.button.y;
					if (flag1 == true)
					{	
						v1.x = x;
						v1.y = y;

						SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
						SDL_RenderClear(ren);

						SDL_SetRenderDrawColor(ren, 0, 87, 249, 255);
						DrawCurve2(ren, v1, v2, v3);
						SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
						SDL_RenderDrawPoint(ren, v1.x, v1.y);
						SDL_RenderDrawPoint(ren, v2.x, v2.y);
						SDL_RenderDrawPoint(ren, v3.x, v3.y);
						SDL_RenderDrawPoint(ren, v4.x, v4.y);

						SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
						SDL_RenderPresent(ren);
						flag1 = false;
					}
					//dem 2
					if (flag2 == true)
					{
						v2.x = x;
						v2.y = y;
						SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
						SDL_RenderClear(ren);

						SDL_SetRenderDrawColor(ren, 0, 87, 249, 255);
						DrawCurve2(ren, v1, v2, v3);
						SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
						SDL_RenderDrawPoint(ren, v1.x, v1.y);
						SDL_RenderDrawPoint(ren, v2.x, v2.y);
						SDL_RenderDrawPoint(ren, v3.x, v3.y);
						SDL_RenderDrawPoint(ren, v4.x, v4.y);

						SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
						SDL_RenderPresent(ren);
						flag2 = false;
					}
					//diem 3
					if (flag3 == true)
					{
						v3.x = x;
						v3.y = y;
						SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
						SDL_RenderClear(ren);

						SDL_SetRenderDrawColor(ren, 0, 87, 249, 255);
						DrawCurve2(ren, v1, v2, v3);
						SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
						SDL_RenderDrawPoint(ren, v1.x, v1.y);
						SDL_RenderDrawPoint(ren, v2.x, v2.y);
						SDL_RenderDrawPoint(ren, v3.x, v3.y);
						SDL_RenderDrawPoint(ren, v4.x, v4.y);
						SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
						SDL_RenderPresent(ren);
						flag3 = false;
					}
					if (flag4 == true)
					{
						v4.x = x;
						v4.y = y;
						SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
						SDL_RenderClear(ren);

						SDL_SetRenderDrawColor(ren, 0, 87, 249, 255);
						DrawCurve2(ren, v1, v2, v3);
						SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
						SDL_RenderDrawPoint(ren, v1.x, v1.y);
						SDL_RenderDrawPoint(ren, v2.x, v2.y);
						SDL_RenderDrawPoint(ren, v3.x, v3.y);
						SDL_RenderDrawPoint(ren, v4.x, v4.y);
						SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
						SDL_RenderPresent(ren);
						flag3 = false;
					}
				}
				break;
			case SDL_MOUSEBUTTONDOWN:

				if (event.button.button == SDL_BUTTON_LEFT)
				{
					int x = event.button.x;
					int y = event.button.y;
					if (x + v1.x < 3 || x - v1.x < 3)
					{
						if (y + v1.y < 3 || y - v1.y < 3)

						{
							flag1 = true;
							flag2 = false;
							flag3 = false;
							flag4 = false;
						}
					}

					//diem 2
					if (x + v2.x < 3 || x - v2.x < 3)
					{
						if (y + v2.y < 3 || y - v2.y < 3)

						{
							flag2 = true;
							flag1 = false;
							flag3 = false;
							flag4 = false;
						}
					}
					////diem 3
					if (x + v3.x < 3 || x - v3.x < 3)
					{
						if (y + v3.y < 3 || y - v3.y < 3)

						{
							flag3 = true;
							flag1 = false;
							flag2 = false;
							flag4 = false;
						}
					}
					if (x + v4.x < 3 || x - v4.x < 3)
					{
						if (y + v4.y < 3 || y - v4.y < 3)

						{
							flag3 = false;
							flag1 = false;
							flag2 = false;
							flag4 = true;
						}
					}
				}
				break;
			}

		}

	}
	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
