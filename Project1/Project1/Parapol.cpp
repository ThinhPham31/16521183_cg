#include "Parapol.h"
#include<iostream>
void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	//draw 2 points
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int p, x, y;
	p = A;
	x = 0;
	y = 0;
	Draw2Points(xc, yc, x, y, ren);
	while(x<=A)
	{ 
		
		if (p < 0)
		{
			p = p + (2*x + 1);
		}
		else
		{
			p = p - 2 * A + (2*x + 1);
			y++;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	p = 2 * x*x + 2 * x + 1 - 4 * A*y;
	while ((x > A) && (y <= 600-yc)) //600 la gioi han cua khung Hello word!!!
	{
		if (p < 0)
		{
			p = p + 4 * A;
		}
		else
		{
			p = p - 4 * x -4 + 4 * A;
			x++;
		}
		y++;
		Draw2Points(xc, yc, x, y, ren);
	}


}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int p, x, y;
	p = -A;
	x = 0;
	y = 0;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A)
	{

		if (p > 0)
		{
			p = p  - (2 * x + 1);
		}
		else
		{
			p = p + 2 * A - (2 * x + 1);
			y--;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	p = 2 * x*x - 2 * x - 1 + 4 * A*y;
	while ((x > A) && (y >-600)) //600 la gioi han cua khung Hello word!!!
	{
		if (p > 0)
		{
			p = p - 4 * A;
		}
		else
		{
			p = p + 4 * x + 4 - 4 * A;
			x++;
		}
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}

	
}